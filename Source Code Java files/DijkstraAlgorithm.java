
package cs542_project;

import static cs542_project.CS542_Project.INFINITY;
import static cs542_project.CS542_Project.distance;
import static cs542_project.CS542_Project.edgeWeight;
import static cs542_project.CS542_Project.num_routers;
import static cs542_project.CS542_Project.predecessor;

/**
 *
 * @author Rohan_Kachewar
 */
public class DijkstraAlgorithm {
 
    public void runDijkstraAlgo(int source) 
    {        
        boolean[] visited = new boolean[INFINITY];      //data structure to indicate nodes visited

        //initialize visited[]
        for (int i = 0; i < num_routers; i++) 
        {
            visited[i] = false;
        }
        //initialize predecessor[][]
        for (int i = 0; i < num_routers; i++) 
        {                        
            if (distance[source][i] > 1000) 
            {
                predecessor[source][i] = -1;
            } 
            else 
            {
                predecessor[source][i] = source;
            }
        }
                         
        //The first node to be visited is the source
        visited[source] = true;
        distance[source][source] = 0;


        for (int count = 1; count <= num_routers - 1; count++) 
        {
            //We search/find a node "k" which has least cost to source
            int k = -1;
            int d_minimum = INFINITY;
            for (int i = 0; i < num_routers; i++) 
            {
                if (!visited[i] && distance[source][i] < d_minimum) 
                {
                    k = i;
                    d_minimum = distance[source][i];
                }
            }

            //We Calculate the shortest path of node k
            visited[k] = true;
            //Path length will be d_minimum
            distance[source][k] = d_minimum;
            

            //adjust all other distances with k as the intermediate node
            for (int i = 0; i < num_routers; i++) 
            {
                if (!visited[i] && distance[k][i] < INFINITY) 
                {
                    d_minimum = distance[source][k] + edgeWeight[k][i];
                }
                if (d_minimum < distance[source][i] && distance[k][i] < INFINITY)
                {
                    distance[source][i] = d_minimum;
                    predecessor[source][i] = k;
                }
            }

        }

    }
    
    
    
}
