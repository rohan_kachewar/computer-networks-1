
package cs542_project;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 *
 * @author Rohan_Kachewar
 * Project compiled using JDK 1.7 (64bit)
 */
public class CS542_Project {
        
    
    static int[][] edgeWeight; //will store the cost of edge
    static int[][] distance;   //stores distance between points
    static int[][] predecessor;       //stores predecessor node of each pair of node
    static int[][] successor;       //stores successor node in the route of each pair of node
    static int[][] temproraryTable;
    static boolean filePresent = false;
    static int num_routers = 0;
    static int INFINITY = 88888;    

    
    public static void main(String[] args) {
                    
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        //CS542_Project helper = new CS542_Project();
        HelperMethods helper = new HelperMethods();
        String fileName = "", option = "";
        int j, k, intChoice, routerNo;
        System.out.println("CS542 Project");
        System.out.println("Author: Rohan Kachewar");
        do 
        {
            //Menu
            System.out.println("CS542 Link State Routing Simulator");
            System.out.println("1. Input Network Topology File");
            System.out.println("2. Build a Connection Table");
            System.out.println("3. Shortest Path to Destination Router"); 
            System.out.println("4. Exit\n\n");
            System.out.println("Command:");            
            try 
            {
                //Read user entered Choice
                option = buf.readLine();
            } 
            catch (IOException e) 
            {
                System.out.println("IO Exception caught");
            }   
            intChoice = Integer.parseInt(option);

            
            if (filePresent == false && intChoice != 1 && intChoice != 4) 
            {
                //if file not present throw error
                System.out.println("File is present, please choose 1 to reload a file: ");
                continue;
            }

            switch (intChoice) 
            {
                case 1:                    
                    //Takes a file and runs dijkstra algorithm on it
                    helper.getInputFileAndRunDijsktra(fileName);
                    break;

                case 2:
                    //Create and Print connection Table
                    helper.createConnectionTable();
                    break;

                case 3:
                    //Get and print out minimum distance path and the distance
                    int source = 0,
                     dest = 0;
                    try 
                    {
                        System.out.println("Select the source and destination router:<source> <destination>");
                        String str = buf.readLine();
                        String[] temp = str.split(" ");

                        source = Integer.parseInt(temp[0]) - 1;                        
                        dest = Integer.parseInt(temp[1]) - 1;

                        helper.printMinDistancePath(source, dest);
                    } 
                    catch (IOException e) 
                    {
                        System.out.println("IO Exception: " + e.getMessage());
                    }
                    break;
                    
                case 4:
                    System.out.println("Exit CS542 project. Good Bye!");
                    System.exit(0);
                    break;
                    
                default:
                    break;
            }
        } while (intChoice != 4);
    }


    
}
