

package cs542_project;

import static cs542_project.CS542_Project.INFINITY;
import static cs542_project.CS542_Project.distance;
import static cs542_project.CS542_Project.edgeWeight;
import static cs542_project.CS542_Project.filePresent;
import static cs542_project.CS542_Project.num_routers;
import static cs542_project.CS542_Project.predecessor;
import static cs542_project.CS542_Project.successor;
import static cs542_project.CS542_Project.temproraryTable;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Rohan_Kachewar
 */
public class HelperMethods {
    
    BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
    int j, k, intChoice, routerNo;
    
    //Function which gets the successive hop from the source router to destination
    public void nextHop(int source) 
    {
        int i;
        for (i = 0; i < num_routers; i++) 
        {
            int temp = predecessor[source][i];
            if (temp == -1) {
                successor[source][i] = -1;
            } 
            else 
            {
                while (temp != source) 
                {
                    successor[source][i] = temp;
                    temp = predecessor[source][temp];
                }
            }
        }
    }

    
    public void createConnectionTable()
    {
        //User input the router number. The index is num-1
        System.out.println("Select a source router:");
        routerNo = verifyRouter(buf) - 1; //check if the number is valid
        System.out.println("The routing table for router " + (routerNo + 1) + "is:");
        //Print the routing table for the selected router
        System.out.println("-----------------");
        System.out.println("Router #  \t Next Hop");
        printnextHop(routerNo);
    }
    
    
    public void getInputFileAndRunDijsktra(String fileName)
    {
         try {
            filePresent = false;  //flag to indicate if file present and opened successfully
            num_routers = 0;
            System.out.println("Input original network topology matix data file: ");
            fileName = buf.readLine();
            FileInputStream fstream = new FileInputStream(fileName);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            filePresent = true;

            //Create the routing table            
            //We assign -1 to edge to indicate INFINITY weight
            while ((strLine = br.readLine()) != null) 
            {
                num_routers++;
            }
            fstream.close();
            in.close();
            br.close();

            temproraryTable = new int[num_routers][num_routers];
            //two arrays which stores the predecessor and successor node of source
            predecessor = new int[num_routers][num_routers];
            successor = new int[num_routers][num_routers]; 
            edgeWeight = new int[num_routers][num_routers];
            distance = new int[num_routers][num_routers];
            j = -1;

            fstream = new FileInputStream(fileName);
            in = new DataInputStream(fstream);
            br = new BufferedReader(new InputStreamReader(in));
            while ((strLine = br.readLine()) != null) 
            {
                j++;
                String[] line = new String[num_routers];
                line = strLine.split(" ");
                for (k = 0; k < num_routers; k++) 
                {
                    temproraryTable[j][k] = Integer.parseInt(line[k]);
                }
            }
            fstream.close();
            in.close();
            br.close();
            
            System.out.println("Original routing table is as follows: ");
            for (j = 0; j < num_routers; j++) 
            {
                for (k = 0; k < num_routers; k++) 
                {
                    System.out.print(temproraryTable[j][k] + " ");
                }
                System.out.println();
            }

            initialize();

            //Building the routing table
            for (j = 0; j < num_routers; j++) 
            {
                DijkstraAlgorithm dijkstra = new DijkstraAlgorithm();                            
                dijkstra.runDijkstraAlgo(j);
            }

            for (j = 0; j < num_routers; j++) 
            {
                nextHop(j);
            }

        } 
        catch (IOException | NumberFormatException e) 
        {
            System.err.println(e.getMessage()); 
            return;
        }


    }
    
            
    //This method print's the connection table wrt source
    public void printnextHop(int source) 
    {
        for (int i = 0; i < num_routers; i++) 
        {
            System.out.print(i + 1 + "\t\t");
            int dist = distance[source][i];
            int weight = edgeWeight[source][i];
            if (dist == weight) {                
                System.out.println("\t" + (i+1)); //if we want the routing table to contain router number even for immediate access
            } 
            else 
            {
                System.out.println("\t" + (successor[source][i] + 1));
            }
        }
        System.out.println();
    }

    /*
    Method to print minimum distance path for given source and destination
    */
    public void printMinDistancePath(int source, int dest) 
    {
        if (source > num_routers || dest > num_routers) 
        {
            System.out.println("Invalid source or destination router specified!!!");            
            return;
        }
        
        
        int[] queue = new int[10000];
        //find path from s to d and store the path in queue
        int i = 0;
        queue[i++] = dest;
        int temp = predecessor[source][dest];
        while (temp != source) 
        {
            queue[i++] = temp;
            temp = predecessor[source][temp];
        }
        //print the path
        int j = 0;
        System.out.print("The shortest path from router " + (source + 1) + " to router " + (dest + 1) + " is: ");
        System.out.print("R" + (source + 1) + "->");
        for (j = i - 1; j > 0; j--) 
        {
            System.out.print("R" + (queue[j] + 1) + "->");
        }
        System.out.print("R" + (dest + 1) + "\n The total cost is (value) " + distance[source][dest] + ".\n");
    }

    //This method verifies if the input number is invalid
    public int verifyRouter(BufferedReader b) 
    {
        int routerNumber = 0;
        String line = "";
        try 
        {
            while (true) 
            {
                line = b.readLine();
                //incorrect intChoice
                if (Integer.parseInt(line) > num_routers) 
                {
                    System.out.println("Invalid router specified!!!");
                    System.out.println("Enter a valid router number(1 to " + num_routers + ") : ");
                    continue;
                } 
                else 
                {
                    break;
                }
            }
        } 
        catch (IOException e) 
        {
            System.out.println(e);
        }
        routerNumber = Integer.parseInt(line);
        return routerNumber;
    }

    //Method o initialize the edgeWeight and distance
    public void initialize() 
    {
        int j, k;
        //Enter the weight of edges in the edgeWeight array
        for (j = 0; j < num_routers; j++) 
        {
            for (k = 0; k < num_routers; k++) 
            {
                if (temproraryTable[j][k] == -1) 
                {
                    edgeWeight[j][k] = INFINITY;
                } 
                else 
                {
                    edgeWeight[j][k] = temproraryTable[j][k];
                }
            }
        }
        
        for (j = 0; j < num_routers; j++) 
        {
            for (k = 0; k < num_routers; k++) 
            {
                distance[j][k] = edgeWeight[j][k];  	 
            }
        }


    }
    
    
}
